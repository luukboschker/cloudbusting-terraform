import { CommandModule } from 'yargs';
import { logger } from '../logger';

const handler = (argv: any) => {
  logger.info(`Got this: ${JSON.stringify(argv, undefined, 2)}`);
};

export const generate: CommandModule = {
  command: 'generate <infraplan>',
  describe: 'Generate terraform files from the specified infraplan file',
  builder: (argv) => argv
    .positional('infraplan', { description: 'The infraplan file to read' })
    .option('output', {
      alias: 'o',
      describe: 'The directory to write the terraform files in',
      default: './out'
    }),
  handler
};
