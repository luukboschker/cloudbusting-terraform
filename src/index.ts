import * as yargs from 'yargs';
import { generate } from './commands/generate';

// eslint-disable-next-line no-unused-vars
const { argv } = yargs
  .command(generate)
  .demandCommand()
  .help();
